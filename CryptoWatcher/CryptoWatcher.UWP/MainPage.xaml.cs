﻿namespace CryptoWatcher.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new CryptoWatcher.App());
        }
    }
}