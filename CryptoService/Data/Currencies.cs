﻿namespace CryptoService.Data
{
    public static class Currencies
    {

        public const string USD = "USD"; // US Dollars
        public const string EUR = "EUR"; // Euros
        public const string CAD = "CAD"; // Canadian Dollars
        public const string AUD = "AUD"; // Australian Dollars

        public static readonly string[] ALL = { USD, EUR, CAD, AUD };

    }
}
