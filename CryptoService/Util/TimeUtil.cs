﻿using System;

namespace CryptoService.Util
{
    public static class TimeUtil
    {

        internal static DateTime EpochToDateTime(int seconds)
        {
            var d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return d.AddSeconds(seconds);
        }

    }
}
