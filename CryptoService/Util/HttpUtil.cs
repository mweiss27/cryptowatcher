﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoService.Util
{
    internal static class HttpUtil
    {

        /// <summary>
        /// Fetches the data from the given URL as a string
        /// </summary>
        /// <param name="url">URL to make a call to</param>
        /// <param name="requestHeaders">Optional, request headers to append to the request.</param>
        /// <returns>string result</returns>
        public static async Task<string> Get(string url)
        {
            var httpClient = new HttpClient();
            return await httpClient.GetStringAsync(url);
        }

    }
}
