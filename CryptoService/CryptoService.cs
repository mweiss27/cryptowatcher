﻿using CryptoService.Models;
using CryptoService.Util;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoService
{
    public static class CryptoService
    {

        const string CoinListUrl = "https://min-api.cryptocompare.com/data/all/coinlist";
        const string CoinCurrentPriceUrl = "https://min-api.cryptocompare.com/data/price?fsym=%0&tsyms=%1";
        const string CoinHistoricalPriceUrl = "https://min-api.cryptocompare.com/data/%0?fsym=%1&tsym=%2&limit=%3&aggregate=%4&e=CCCAGG";

        public delegate void CryptoCoinListCallback(CoinList coinList);

        public static async Task<CoinList> GetCoinList()
        {
            return await Task.Run(async () =>
            {
                var response = await HttpUtil.Get(CoinListUrl);
                var json = JObject.Parse(response);

                var coinList = new CoinList
                {
                    Response = json["Response"]?.ToString(),
                    ResponseCode = int.Parse(json["Type"]?.ToString()),
                    BaseImageUrl = json["BaseImageUrl"]?.ToString(),
                    BaseLinkUrl = json["BaseLinkUrl"]?.ToString()
                };

                var coins = new List<Coin>();

                var data = json["Data"];
                var children = data.Children();
                foreach (var child in children)
                {
                    var info = child.First;
                    var coin = new Coin
                    {
                        Id = int.Parse(info["Id"]?.ToString()),
                        Url = info["Url"]?.ToString(),
                        ImageUrl = info["ImageUrl"]?.ToString(),
                        Name = info["Name"]?.ToString(),
                        CoinName = info["CoinName"]?.ToString(),
                        FullName = info["FullName"]?.ToString(),
                        Algorithm = info["Algorithm"]?.ToString(),
                        ProofType = info["ProofType"]?.ToString(),
                        SortOrder = int.Parse(info["SortOrder"]?.ToString())
                    };

                    coins.Add(coin);
                }
                coinList.Coins = coins;

                return coinList;
            });
        }
        
        /// <summary>
        /// Fetches the current price for the given coin. The price is converted to all currencies provided.
        /// </summary>
        /// <param name="coin">string representation for the coin. BTC, for example</param>
        /// <param name="currencies">An array of currencies. [USD, EUR], for example. See the Currencies class</param>
        /// <returns>A dictionary with values for each provided currency</returns>
        public static async Task<Dictionary<string, double>> GetCurrentPrices(string coin, string[] currencies)
        {
            return await Task.Run(async () =>
            {
                var url = BuildUrl(CoinCurrentPriceUrl, new[] { coin.ToUpper(), string.Join(",", currencies).ToUpper() });

                var response = await HttpUtil.Get(url);
                var json = JObject.Parse(response);

                var result = new Dictionary<string, double>();

                foreach (var currency in currencies)
                {
                    var price = json[currency];
                    result.Add(currency, double.Parse(price.ToString()));
                }

                return result;
            });
            
        }

        /// <summary>
        /// Fetches historical pricing data for the given coin and currency.
        /// </summary>
        /// <param name="type">Daily or Hourly</param>
        /// <param name="coin">Coin  type to fetch. BTC, for example</param>
        /// <param name="currency">Type of currency to fetch. USD, for example</param>
        /// <param name="limit">Number of entries. Max 2000</param>
        /// <param name="aggregate">How many days to aggregate. e.g. limit=10, aggregate=3, will result in 10 entries, but spans data for 30 days</param>
        /// <returns>A collection of pricing data objects</returns>
        public static async Task<IEnumerable<CoinPriceHistorical>> GetHistoricalPrices(HistoricalType type, string coin, string currency, int limit, int aggregate)
        {
            return await Task.Run(async () =>
            {
                var type_s = type == HistoricalType.Daily ? "histoday" : "histohour";
                var url = BuildUrl(CoinHistoricalPriceUrl, new[] { type_s, coin, currency, limit.ToString(), aggregate.ToString() });

                var response = await HttpUtil.Get(url);
                var json = JObject.Parse(response);

                var data = json["Data"];
                var result = new List<CoinPriceHistorical>();

                foreach (var child in data.Children())
                {
                    var price = new CoinPriceHistorical
                    {
                        Time = int.Parse(child["time"]?.ToString()),
                        Close = double.Parse(child["close"]?.ToString()),
                        Open = double.Parse(child["open"]?.ToString()),

                        Low = double.Parse(child["low"]?.ToString()),
                        High = double.Parse(child["high"]?.ToString()),

                        VolumeFrom = double.Parse(child["volumefrom"]?.ToString()),
                        VolumeTo = double.Parse(child["volumeto"]?.ToString())
                    };

                    result.Add(price);
                }

                return result.OrderBy(cph => cph.Time).Reverse();
            });
        }

        // Replaces any % token in the provided url with the matching value provided.
        static string BuildUrl(string url, string[] values)
        {
            for (var i = 0; i < values.Length; i++)
                url = url.Replace("%" + i, values[i]);

            return url;
        }

    }
}
