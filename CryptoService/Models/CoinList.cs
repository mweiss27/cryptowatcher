﻿using System.Collections.Generic;

namespace CryptoService.Models
{
    public class CoinList
    {

        public string Response { get; set; } // Message from the API indicating success or failure
        public int ResponseCode { get; set; } // Response code. API uses their own for some reason, 100 indicates success
        public string Message { get; set; } // Message from the API, details about response status
        public string BaseImageUrl { get; set; } // Base path to image for this coin type
        public string BaseLinkUrl { get; set; } // Base path for overview page for this coin type
        public IList<Coin> Coins { get; set; } // Collection of coins
    }
}
