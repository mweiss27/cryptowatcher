﻿namespace CryptoService.Models
{
    public class Coin
    {
        public int Id { get; set; } // ID of cryptocurrency type. 1182 for BTC, for example
        public string Url { get; set; } // Url to overview of this coin type. Relative to BaseUrl from CoinList
        public string ImageUrl { get; set; } // Url to an image of some sort
        public string Name { get; set; } // Abbreviated name of this coin type. BTC, for example
        public string CoinName { get; set; } // Proper name of this coin type. Bitcoin, for example
        public string FullName { get; set; } // Full name of this coin type. Bitcoin (BTC), for example
        public string Algorithm { get; set; }
        public string ProofType { get; set; }
        public int SortOrder { get; set; }
    }
}
